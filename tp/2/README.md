# TP2 : Déploiement automatisé

Le but de ce TP est d'effectuer le même déploiement que lors du [TP1](../1/) mais en automatisant le déploiement de la machine virtuelle, sa configuration basique, ainsi que l'install et la conf des services.

Au menu :
* réutilisation du [TP1](../1/)
* utilisation de [Vagrant](https://www.vagrantup.com/)
* premiers pas dans l'automatisation

<!-- vim-markdown-toc GitLab -->

* [0. Prérequis](#0-prérequis)
    * [Install Vagrant](#install-vagrant)
    * [Init Vagrant](#init-vagrant)

<!-- vim-markdown-toc -->

# 0. Prérequis

## Install Vagrant

Téléchargez [Vagrant](https://www.vagrantup.com/) depuis le site officiel. Une fois téléchargé, assurez-vous que vous avez la commande `vagrant` dans votre terminal.

Vous aurez aussi besoin de VirtualBox. **Je n'apporterai aucun support si vous utilisez un autre hyperviseur.**

Vagrant est un outil qui sert de surcouche à un hyperviseur ; dans notre cas, il pilotera VirtualBox. 

Le fonctionnement de Vagrant est simple :
* on décrit une ou plusieurs VM(s) dans un fichier appelé `Vagrantfile`
* on demande à Vagrant d'allumer la ou les VM(s)

La description des VMs se fait dans un langage spécifique, dérivé de Ruby.

## Init Vagrant

```
# Créez vous un répertoire de travail
$ mkdir vagrant
$ cd vagrant

# Initialisez un Vagrantfile
$ vagrant init centos/7
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

> Je vous invite à **lire** le fichier Vagrantfile qui a été généré automatiquement pour voir une partie de ce que Vagrant est capable de réaliser.

Une fois le Vagrantfile généré, épurez-le en enlevant les commentaires, et ajoutez des lignes afin qu'il ressemble à ça :

```
Vagrant.configure("2")do|config|
  config.vm.box="centos/7"

  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
  config.vbguest.auto_update = false

  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 

  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true
end
```

> Si vous avez l'erreur `Unknown configuration section 'vbguest'`, lancez la commande `vagrant plugin install vagrant-vbguest` AVANT le `vagrant up`.

Test du bon fonctionnement :
```bash
# Toujours dans le dossier où a été généré le Vagrantfile
$ vagrant up
[...]

# Voir l'état de la machine
$ vagrant status
# Vous pouvez aussi jeter un oeil dans votre VirtualBox : une VM devrait avoir pop

# Se connecter à la machine
$ vagrant ssh

# Détruire la VM et les fichiers associés
$ vagrant destroy -f
```

